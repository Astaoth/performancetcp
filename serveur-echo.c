#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

/*struct sockaddr
{
  u_short sa_family;//La famille de protocole : IP, UNIX ...
  char sa_data[14];//L'adresse (son format dépend du protocole précédent)
}*/



int main() 
  {
    struct sockaddr_in sockclient;
    struct sockaddr_in sock;
    int fds, lensock;

    fds=socket(AF_INET, SOCK_STREAM, 0);
    if(fds==-1)
      {
	perror("Error Socket() : ");
	exit(-1);
      }
    sock.sin_family=AF_INET;
    sock.sin_port=htons(5001);
    sock.sin_addr.s_addr=htonl(INADDR_ANY);
    if(bind (fds, (struct sockaddr *) &sock, sizeof(sock))==-1)
      {
	perror("Error bind() : ");
	exit(-2);
      }
      if(listen(fds, 10)==-1)
	{
	  perror("Error listen() : ");
	  exit(-3);
	}

      while(1)
	{
	  lensock=sizeof(struct sockaddr);
	  int acc=accept(fds, (struct sockaddr*) &sockclient, &lensock);
	  if(acc==-1)
	    {
	      perror("Error Accept()");
	      exit(-4);
	    }

	  int nb, buf_len=512;
	  char buffer[512];
	  nb=read(acc, &buffer, buf_len);
	  write(acc, &buffer, nb);
	  close(acc);
	} //Fin du while

      close(fds);
      exit(0);
  }
